This repository aims to illustrate the following article on Linux.Fr.org:
https://linuxfr.org/redaction/news/predire-la-note-d-un-journal

You can explore the data by running the following ipython file diaries_classification.ipynb :

```
jupyter notebook
```
