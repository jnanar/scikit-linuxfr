#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of scikit-learn_for_linuxfr.
#
# scikit-learn_for_linuxfr is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# scikit-learn_for_linuxfr is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with scikit-learn_for_linuxfr.  If not, see <http://www.gnu.org/licenses/>.
#

from bs4 import BeautifulSoup
import urllib3
import html2text
import csv

import logging

logging.basicConfig()
logger = logging.getLogger("linuxfr_parser")
logger.setLevel("DEBUG")


class LinuxFR:
    """
    This class aims to save diaries entry from linuxfr.org into CSV files.
    """

    def __init__(self, base_url):
        self.filename = ''
        self.known_url = []
        self.known_authors = []

    def set_filename(self, filename):
        self.filename = filename

    def get_urls(self):
        return self.known_url

    def get_authors(self):
        return self.known_authors

    def cat_category(self, parameters):
        boundaries = [-20, 0, 20]
        if parameters['score'] <= min(boundaries):
            # Score < -50
            parameters['quality_content'] = 'Magnificent Troll'
        if parameters['score'] >= max(boundaries):
            # Score > 50
            parameters['quality_content'] = 'Quality Troll'
        if 0 < parameters['score'] < max(boundaries):
            # 0 < Score < 50
            parameters['quality_content'] = 'Average Troll'
        if 0 >= parameters['score'] > min(boundaries):
            # 0 > Score > -50
            parameters['quality_content'] = 'Great Troll'
        return parameters['quality_content']

    def open_csv(self):
        """
        Open a csv file (self.filename) and read the urls of diaries entries. It prevent to parse mutiple times the same pages.
        :return: True (success) or False
        """
        try:
            with open(self.filename, 'r') as csvfile:
                saved_diaries = csv.DictReader(csvfile, delimiter='£', quotechar='µ')
                for row in saved_diaries:
                    try:
                        self.known_url.append(row['url'])
                    except KeyError:
                        logger.debug("KeyError : key does not exist")
                        pass
            return True
        except FileNotFoundError:
            logger.error("CSV Python file not found")
            return False

    def prepare_csv(self, base_url='', diaries=[]):
        """
        Write the data into CSV file. The columns are "fieldname".
        :param base_url: the base url: https://linuxfr.org
        :param diaries: the list of diaries
        :return: fieldnames of the CSV file
        """
        # We append the new lines to CSV
        fieldnames = ['title', 'author', 'url', 'score', 'content', 'quality_content', 'count', 'datetime',
                      'author_url', 'author_previous_scores', 'birthday', 'n_comments', 'comments_scores']
        with open(self.filename, 'a') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='£', quotechar='µ')
            writer.writeheader()
            for row in diaries:
                if row['quality_content'] == '':
                    row['quality_content'] = self.cat_category(row)
                complete_url = base_url + row['url']
                if complete_url not in self.known_url:
                    logger.debug("Append url {} to file".format(row['url']))
                    writer.writerow(row)
                else:
                    logger.debug("URL {} already in the CSV file.".format(row['url']))


def get_soup(path):
    ca_certs = "/etc/ssl/certs/ca-certificates.crt"  # Or wherever it lives.

    http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',
                               ca_certs=ca_certs, )
    req = http.request('GET', path)
    req_html = req.data
    req.close()
    return req_html


def parse_journals_list(base_url='', count=0):
    """
    Parse the list of diaries on the main page
    :param base_url: 
    :param count: 
    :return: 
    """
    diaries_urls = []
    path = base_url + '/journaux?page=' + str(count)
    diaries_html = get_soup(path)
    soup = BeautifulSoup(diaries_html, "lxml")

    for link in soup.find_all('a'):
        try:
            if '/users/' in link.attrs['href'] and 'Lire la suite' in link.contents:
                try:
                    if link.attrs['href'] not in diaries_urls:
                        diaries_urls.append(link.attrs['href'])
                except (TypeError, KeyError) as err:
                    logger.error(err)
                    logger.error(err.args)
                    pass
        except KeyError as err:
            logger.error(err)
            logger.error(err.args)

    return diaries_urls


def extract_user_data(base_url='', url=''):
    """
    extract a list of previous scores
    :param base_url: 
    :param url: the partial url of the user
    :return: a list of scores
    """
    user_birthday = None
    list_scores = []
    path = base_url + url
    soup = BeautifulSoup(get_soup(path), "lxml")
    content = soup.select('div#user_recent_contents')
    ps = soup.find_all('p')
    for p in ps:
        if "Compte créé le" in p.next_element:
            user_birthday = str(p.next_element)
            user_birthday = user_birthday.replace("Compte créé le ", '')
            break

    content_scores = soup.find_all("figure", class_="score")
    for entry in content_scores:
        entry = entry.contents
        score = float(entry[0])
        list_scores.append(score)

    return list_scores, user_birthday


def extract_data(soup=None, div_class=None, figure_class=None, meta_content=None, datetime=None,
                 comments=False):
    """
    Extract data from the diary page
    :param datetime: 
    :param soup: 
    :param div_class: 
    :param figure_class: 
    :param meta_content: 
    :return: 
    """
    content_string = ''
    content = ''
    if div_class:
        content = soup.find("div", class_=div_class)
    if figure_class:
        content = soup.find("figure", class_=figure_class)
    if meta_content:
        # Find name: meta_content = "author"
        content = soup.find("meta", {"name": meta_content})['content']
        a = 0
    if datetime:
        datetime = False
        content = soup.find("div", class_="meta")
        content = content.contents
        soup_datetime = None

        content_string = str(content)
        # except AttributeError:
        #    content_string = content
        #    logger.error("DATETIME")
        soup_datetime = BeautifulSoup(content_string, "lxml")
        if soup_datetime:
            for element in soup_datetime.findAll('time'):
                if element.has_attr('datetime'):
                    datetime = element['datetime']
        return datetime

    content_string = str(content)
    if comments:
        n_comments = 0
#        itemprops = soup.find_all("meta", {"itemprop": 'interactionCount'})
#        for item in itemprops:
#            if 'UserComments:' in item.attrs['content']:
#                n_comments = item.attrs['content']
#                n_comments = n_comments.replace('UserComments:', '')
#                n_comments = float(n_comments)
        comments= soup.find_all("span", class_="score")
        scores = []
        for comment in comments:
            score = comment.contents
            score = float(score[0])
            scores.append(score)
        n_comments = len(scores)




        return n_comments, scores
    try:
        content_string = html2text.html2text(content_string)
    except TypeError:
        logger.error("Content error")
    return content_string


def collect_diaries(base_url='https://linuxfr.org', diaries_urls=[], known_urls=[], lf_instance=None):
    """
    Get the content of the diary from its page (URL contains the authors's name)
    KNOWN BUG: authors pages are parsed multiple times
    TODO: a dictionnary of known autohors with their publication history
    :param base_url: 
    :param diaries_urls: 
    :param known_urls: 
    :return: 
    """
    diaries = []

    for url in diaries_urls:
        i = 1
        if url not in known_urls:
            logger.info("{} Extract data from {}".format(i,url))
            path = base_url + url
            bs = BeautifulSoup(get_soup(path), "lxml")
            content_string = extract_data(soup=bs, div_class='content entry-content')
            score = extract_data(soup=bs, figure_class='score')
            score = float(score.replace("Note de ce contenu", ''))
            author = extract_data(soup=bs, meta_content='author')
            author = author.replace('\n', '')
            title = extract_data(soup=bs, meta_content='description')
            title = title.replace('\n', '')
            datetime = extract_data(soup=bs, datetime=True)
            author_url = url.split("/journaux/")[0]
            author_previous_scores, birthday = extract_user_data(base_url=base_url, url=author_url)
            n_comments,  comments_scores= extract_data(soup=bs, comments=True)
            diary_dict = {'url': url, 'content': content_string, 'score': score,
                          'title': title, 'author': author, 'quality_content': '', 'count': len(content_string),
                          'datetime': datetime, 'author_url': base_url+author_url, 'author_previous_scores': author_previous_scores,
                          "birthday":birthday, 'n_comments': n_comments, 'comments_scores': comments_scores}
            diaries.append(diary_dict)
            i += 1
    return diaries


def launcher():
    base_url = 'https://linuxfr.org'
    diaries_urls = []
    lf = LinuxFR(base_url)
    lf.set_filename('linuxfr_total.csv')
    parse_without_content = True
    lf.open_csv()
    known_urls = lf.get_urls()
    # 1 --> 100 OK
    # 100 --> 200 OK
    # 200 --> 300 OK
    # 300 --> 400 OK
    # 400 --> 500 :OUT of SAMPLE
    # Go up to 670, after that, it seems to be score = 0 all the time
    for count in range(1, 2):
        logger.info("Page : {}".format(count))
        diaries_urls += parse_journals_list(base_url=base_url, count=count)
    logger.info("{} diaries ".format(len(diaries_urls)))
    # Collect the diaries content, user, score etc (!!! no datetime)
    diaries = collect_diaries(base_url=base_url, diaries_urls=diaries_urls, known_urls=known_urls, lf_instance=lf)
    lf.prepare_csv(base_url=base_url, diaries=diaries)


if __name__ == "__main__":
    launcher()
